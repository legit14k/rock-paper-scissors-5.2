//
//  ViewController.swift
//  GrimbergJason_5.2
//
//  Created by Jason Grimberg on 11/5/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ViewController: UIViewController, MCBrowserViewControllerDelegate, MCSessionDelegate {
    // All timer information
    var timer = Timer()
    var timerIsOn = false
    var timeRemaining = 4
    
    // MARK: - Outlets
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var winScoreLabel: UILabel!
    @IBOutlet weak var tieScoreLabel: UILabel!
    @IBOutlet weak var losesScoreLabel: UILabel!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var picture0: UIImageView!
    @IBOutlet weak var picture1: UIImageView!
    @IBOutlet weak var picture2: UIImageView!
    @IBOutlet weak var countDown: UILabel!
    @IBOutlet weak var connectionLabel: UILabel!
    @IBOutlet weak var user1Image: UIImageView!
    @IBOutlet weak var user2Image: UIImageView!
    @IBOutlet var allChoices: [UIImageView]!
    @IBOutlet weak var yourChoiceLabel: UILabel!
    @IBOutlet weak var opponentsChoiceLabel: UILabel!
    @IBOutlet weak var connectButton: UIBarButtonItem!
    
    /* The four main building blocks of a Multipeer app */
    var peerID: MCPeerID! // Our device's ID or name as viewed by other 'browsing' devices.
    var session: MCSession! // The 'connection' between devices
    var browser: MCBrowserViewController! // Prebuilt ViewController that searches for nearby advertisers.
    var advertiser: MCAdvertiserAssistant! // Help us easily advertise ourselves to nearb MCBrowsers.
    
    var discon = 0
    var winScore = 0
    var loseScore = 0
    var tieScore = 0
    var win = "You Win!"
    var lose = "You Lose!"
    var tie = "You Tied!"
    var readyUp = 0
    var yourChoice = ""
    var opponentsChoice = ""
    var starting = "start"
    let serviceID = "channelID" // 'Channel' or service type
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add tap gestures to all of the choices
        for i in 0..<(allChoices.count) {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self,
                    action: #selector(imageTapped(tapGestureRecognizer:)))
            
            allChoices[i].isUserInteractionEnabled = true
            allChoices[i].addGestureRecognizer(tapGestureRecognizer)
        }
        
        // Set the button text when game first starts
        playButton.setTitle("Play", for: .normal)
        
        // Change the navigation title
        navItem.title = "Status: Not Connected"
        
        // Hide the connection label
        self.connectionLabel.isHidden = true
        
        // Hide the countdown time label
        countDown.isHidden = true
        
        // Change the name of the labels
        yourChoiceLabel.text = "Your choice"
        opponentsChoiceLabel.text = "Opponents choice"
        
        // Hide all the imageViews
        picture0.isHidden = true
        picture1.isHidden = true
        picture2.isHidden = true
        user2Image.isHidden = true
        
        // Hide and make the button not available
        playButton.isEnabled = false
        playButton.isHidden = true
        
        // Setup our MC objects
        peerID = MCPeerID(displayName: UIDevice.current.name)
        
        // Use PeerID to setup a session
        session = MCSession(peer: peerID)
        session.delegate = self
        
        // Setup and start advertising immediately
        advertiser = MCAdvertiserAssistant(serviceType: serviceID, discoveryInfo: nil, session: session)
        advertiser.start()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - MCBrowserViewControllerDelegate Callback Methods
    // Notifies the delegate, when the user taps the done button.
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        browserViewController.dismiss(animated: true, completion: nil)
    }
    
    // Notifies delegate that the user taps the cancel button.
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        browserViewController.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - MCSessionDelegate callbacks
    // Remote peer changed state.
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        
        /* This whole callback happens in a background thread */
        DispatchQueue.main.async {
            
            if state == MCSessionState.connected {
                if session.connectedPeers.count > 1 {
                    self.navItem.title = "Status: \(session.connectedPeers.count) Connections"
                    self.infoLabel.isHidden = true
                    self.playButton.isHidden = false
                    self.playButton.isEnabled = true
                }
                else {
                    self.navItem.title = "Status: Connected"
                    self.connectButton.title = "Disconnect"
                    self.infoLabel.isHidden = true
                    self.playButton.isHidden = false
                    self.playButton.isEnabled = true
                }
                
            } else if state == MCSessionState.connecting {
                self.navItem.title = "Status: Connecting"
                
            } else if state == MCSessionState.notConnected {
                self.navItem.title = "Status: Not Connected"
                self.connectButton.title = "Connect"
                self.playButton.setTitle("Play", for: .normal)
                self.infoLabel.isHidden = false
                self.playButton.isHidden = true
                self.playButton.isEnabled = false
                self.picture0.isHidden = true
                self.picture1.isHidden = true
                self.picture2.isHidden = true
                self.user1Image.isHidden = true
                self.user2Image.isHidden = true
                self.countDown.isHidden = true
                self.winScore = 0
                self.tieScore = 0
                self.loseScore = 0
                self.winScoreLabel.text = self.winScore.description
                self.losesScoreLabel.text = self.loseScore.description
                self.tieScoreLabel.text = self.tieScore.description
            }
        }
    }
    
    // Received data from remote peer.
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        
        // Build new String from the encoded NSData Object. (UnEncoding)
        // String if the opponent is ready and hit play
        if let userReady: String = String(data: data, encoding: String.Encoding.utf8) {
            readyUp = Int(userReady)!
        }
        
        // String of what was chosen
        if let usersChoice: String = String(data: data, encoding: String.Encoding.utf8) {
            opponentsChoice = usersChoice
        }
        
        DispatchQueue.main.async {
            // Display what user is ready
            switch self.readyUp {
            case 1:
                // Show who you are waiting for
                self.connectionLabel.isHidden = false
                self.connectionLabel.text = "Opponent ready..."
            case 2:
                // Start a new game when both are ready
                self.startNewGame()
            default:
                print("Should not reach here: received.")
            }
        }
    }
    
    // Received a byte stream from remote peer.
    func session(_ session: MCSession, didReceive stream: InputStream,
                 withName streamName: String, fromPeer peerID: MCPeerID){}
    
    // Start receiving a resource from remote peer.
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String,
                 fromPeer peerID: MCPeerID, with progress: Progress) {}
    
    // Finished receiving a resource from remote peer and saved the content
    // in a temporary location - the app is responsible for moving the file
    // to a permanent location within its sandbox.
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String,
                 fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {}
    
    // MARK: - Actions
    @IBAction func connectTapped(_ sender: Any) {
        if discon == 1 {
            func disconnect(){
                discon = 0
            }
        } else {
        // Our browser will look for advertisers that share the same service ID
        browser = MCBrowserViewController(serviceType: serviceID, session: session)
        browser.delegate = self
        // Present the browser
        self.present(browser, animated: true, completion: nil)
        // Set the disconnect to 1 for reset
        discon = 1
        }
    }
    
    @IBAction func playTapped(_ sender: Any) {
        // Hide what doesn't need to be shown
        user2Image.isHidden = true
        user1Image.isHidden = true
        countDown.isHidden = true
        
        // Hide play button after tapped
        playButton.isHidden = true
        
        // Disable the play button
        self.playButton.isEnabled = false
        
        // Reset the ready up after every game
        if readyUp >= 2 {
            // Reset the ready up
            readyUp = 0
            // Reset the timer
            timeRemaining = 4
            // Add one to the count
            readyUp = readyUp + 1
            
            // Send the count over to the other user
            if let readyUpString = readyUp.description.data(using: String.Encoding.utf8) {
                do {
                    try session.send(readyUpString, toPeers: session.connectedPeers,
                            with: MCSessionSendDataMode.reliable)
                } catch {
                    print("Error: Send data failed.")
                }
            }
            
            // See if you need to start the game or not
            switch readyUp {
            case 1:
                // Show who you are waiting for
                self.connectionLabel.isHidden = false
                self.connectionLabel.text = "Waiting on opponent..."
            case 2:
                // Start a new game when both are ready
                startNewGame()
            default:
                print("Should not reach this point in ready up if.")
            }
        } else {
            // Add one to the count
            readyUp = readyUp + 1
            
            // Reset the timer
            timeRemaining = 4
            
            // Send the count over to the other user
            if let readyUpString = readyUp.description.data(using: String.Encoding.utf8) {
                do {
                    try session.send(readyUpString, toPeers: session.connectedPeers,
                            with: MCSessionSendDataMode.reliable)
                } catch {
                    print("Error: Send data failed.")
                }
            }
            
            // See if you need to start the game or not
            switch readyUp {
            case 1:
                // Show who you are waiting for
                self.connectionLabel.isHidden = false
                self.connectionLabel.text = "Waiting on opponent..."
            case 2:
                // Start a new game when both are ready
                startNewGame()
            default:
                print("Should not reach this point in ready up")
            }
        }
    }
    
    func startNewGame() {
        // Show the pictures of what you can choose
        self.picture0.isHidden = false
        self.picture1.isHidden = false
        self.picture2.isHidden = false
        // Enable the user interaction for what you can choose
        self.picture0.isUserInteractionEnabled = true
        self.picture1.isUserInteractionEnabled = true
        self.picture2.isUserInteractionEnabled = true
        countDown.isHidden = false
        
        // Clear the countdown text
        countDown.text = ""
        
        // Reset the timer
        timeRemaining = 4
        
        // Start the timer back up
        timer.fire()
        
        // Reset the count
        readyUp = 0
        
        // Hide the label
        self.connectionLabel.isHidden = true
        
        // UI Stuff on main thread
        DispatchQueue.main.async {
            if self.starting == "start" {
                if !self.timerIsOn {
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self,
                        selector: #selector(self.timerRunning), userInfo: nil, repeats: true)
                    // Keep the timer on until 0
                    self.timerIsOn = true
                }
            }
        }
    }
    
    func checkCurrenGame() {
        /*
         3 - Rock
         4 - Paper
         5 - Scissors
         */
        // Change the play button text after the first game
        // so the users will know they can play again
        playButton.setTitle("Play again?", for: .normal)
        
        // Show the opponents choice
        user2Image.isHidden = false
        
        // Hide the choices
        self.picture0.isHidden = true
        self.picture1.isHidden = true
        self.picture2.isHidden = true
        // Lock in the users choice
        self.picture0.isUserInteractionEnabled = false
        self.picture1.isUserInteractionEnabled = false
        self.picture2.isUserInteractionEnabled = false
        
        // Change the picture of what the opponent chose
        switch opponentsChoice {
        case "3":
            user2Image.image = #imageLiteral(resourceName: "0_rock")
        case "4":
            user2Image.image = #imageLiteral(resourceName: "1_paper")
        case "5":
            user2Image.image = #imageLiteral(resourceName: "2_scissors")
        default:
            print("Should not reach here in current check")
        }
        // Win statements
        // Rock beats scissors
        if yourChoice == "3" && opponentsChoice == "5" {
            // Show win text
            countDown.text = win
            
            // Add to the win score
            winScore = winScore + 1
            
            // Update the win score label
            winScoreLabel.text = winScore.description
        } else if opponentsChoice == "3" && yourChoice == "5" {
            // Show loss text
            countDown.text = lose
            
            // Add to the loss score
            loseScore = loseScore + 1
            
            // Update te loss label
            losesScoreLabel.text = loseScore.description
        }
        
        // Scissors beats paper
        if yourChoice == "5" && opponentsChoice == "4" {
            // Show win text
            countDown.text = win
            
            // Add to the win score
            winScore = winScore + 1
            
            // Update the win score label
            winScoreLabel.text = winScore.description
        } else if opponentsChoice == "5" && yourChoice == "4" {
            // Show loss text
            countDown.text = lose
            
            // Add to the loss score
            loseScore = loseScore + 1
            
            // Update te loss label
            losesScoreLabel.text = loseScore.description
        }
        
        // Paper covers rock
        if yourChoice == "4" && opponentsChoice == "3" {
            // Show win text
            countDown.text = win
            
            // Add to the win score
            winScore = winScore + 1
            
            // Update the win score label
            winScoreLabel.text = winScore.description
        } else if opponentsChoice == "4" && yourChoice == "3" {
            // Show loss text
            countDown.text = lose
            
            // Add to the loss score
            loseScore = loseScore + 1
            
            // Update te loss label
            losesScoreLabel.text = loseScore.description
        }
        
        // Check if you have tied
        if yourChoice == opponentsChoice {
            // Show tie text
            countDown.text = tie
            
            // Add to the tie score
            tieScore = tieScore + 1
            
            // Update the tie label
            tieScoreLabel.text = tieScore.description
        }
        // Show the play button
        self.playButton.isHidden = false
        
        // Enable the play button for next game
        self.playButton.isEnabled = true
    }
    
    @objc func timerRunning() {
        // Show the countdown timer
        countDown.isHidden = false
        
        // Minus one from the time remaining
        timeRemaining -= 1
        
        if timeRemaining >= 1 {
            // Set the time remaining in seconds
            let secondsLeft = (Int(timeRemaining) % 60)
            
            // Change the countdown text
            countDown.text = "\(secondsLeft)..."
            
        } else if timeRemaining == 0 {
            // Set the countdown instead of 0 set to shoot
            countDown.text = "Shoot!"
            
        } else if timeRemaining <= 0 {
            // Turn off the timer
            timerIsOn = false
            timer.invalidate()
            
            // Check the current game state
            checkCurrenGame()
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        // Show what you have chosen
        user1Image.isHidden = false
        
        // Change the image to what you have chosen
        switch tappedImage.tag {
        case 3:
            yourChoice = 3.description
            user1Image.image = #imageLiteral(resourceName: "0_rock")
        case 4:
            yourChoice = 4.description
            user1Image.image = #imageLiteral(resourceName: "1_paper")
        case 5:
            yourChoice = 5.description
            user1Image.image = #imageLiteral(resourceName: "2_scissors")
        default:
            print("Should not reach this point in image tapped.")
        }
        
        // Send the count over to the other user
        if let tappedImageString = tappedImage.tag.description.data(using: String.Encoding.utf8) {
            do {
                try session.send(tappedImageString, toPeers: session.connectedPeers, with: MCSessionSendDataMode.reliable)
            } catch {
                print("Error: Send data failed.")
            }
        }
    }
}

